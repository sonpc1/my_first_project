import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
import datetime
from my_first_project.api_cls import Method

def drawchart_pd():
    mpl.style.use('seaborn')
    wallet = ["Treasury", "Huobi", "Binance"]
    wal_name = []
    number = []
    date_time = []
    for x in wallet:
        url = 'http://210.211.97.169:8061/wallet_number/?where={"wallet": "%s"}' % x
        wal_name += Method.getdata(url)[2]
        number += Method.getdata(url)[0]
        date_time = Method.getdata(url)[1]
    date_time = date_time * 3
    for row in range(0, len(date_time)):
        x = date_time[row][0:16]
        x = datetime.datetime.strptime(x, '%a, %d %b %Y').strftime('%d/%m/%Y')
        date_time[row] = x
    print(date_time)
    df = pd.DataFrame({'wallet': wal_name, 'date_time': date_time, 'number': number})
    print(df.head(len(wal_name)))
    df_huobi = df[df.wallet == "Huobi"].sort_values('date_time', ascending=True)
    df_treasury = df[df.wallet == "Treasury"].sort_values('date_time', ascending=True)
    df_binance = df[df.wallet == "Binance"].sort_values('date_time', ascending=True)

    plt.plot(df_huobi.date_time, df_huobi.number, label='Huobi')
    plt.plot(df_treasury.date_time, df_treasury.number, label='Treasury')
    plt.plot(df_binance.date_time, df_binance.number, label='Binance')

    plt.title('Tester')
    plt.legend()
    plt.savefig('test1.png')
    print(df.columns)