import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl

x = [1, 2]
x[0] = 3
print(x)
'''
mpl.style.use('seaborn')

df = pd.DataFrame({
'wallet' : ['Huobi', 'Bittrex', 'Bitfinex', 'Huobi', 'Bittrex', 'Bitfinex'],
'date_time': ['2020-02-02','2020-02-02','2020-02-02', '2020-02-03','2020-02-03','2020-02-03'],
'number': [1000, 2000, 3000, 2000, 4000, 6000]
})
print(df.head())
df_huobi = df[df.wallet == "Huobi"].sort_values('date_time', ascending=True)
df_bittrex = df[df.wallet == "Bittrex"].sort_values('date_time', ascending=True)
df_bitfinex = df[df.wallet == "Bitfinex"].sort_values('date_time', ascending=True)

print(df_huobi)
print(df_bittrex)
print(df_bitfinex)

plt.plot(df_huobi.date_time, df_huobi.number, label='Huobi')
plt.plot(df_bittrex.date_time, df_bittrex.number, label='Bittrex')
plt.plot(df_bitfinex.date_time, df_bitfinex.number, label='Bitfinex')

plt.title('Tester')
plt.legend()
plt.savefig('test.png')
print(df.columns)
'''

