from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from my_first_project.editdb import wallet
import requests
import time
import datetime
import os
import platform


n = 0
cap = [0, 0]
url0 = "https://tether.to/"
url1 = "https://wallet.tether.to/richlist"
url2= "https://wallet.tether.to/transparency"



class Crawler:
    def __init__(self):
        pass

    def init_driver(self):
        # get file chrome driver
        dir_path = os.path.dirname(os.path.realpath(__file__))
        #pth_help = os.path.join(dir_path, "..")
        #pth_help = os.path.join(pth_help, "..")
        pth_help = os.path.join(dir_path, "helpers")
        pth_dri = os.path.join(pth_help, "driver")
        driver = None
        if platform.system() == 'Windows':
            pth_chrm = os.path.join(pth_dri, "chromedriver_72.exe")
            driver = webdriver.Chrome(pth_chrm)
        elif platform.system() == 'Linux':
            pth_geck = os.path.join(pth_dri, "geckodriver_24")
            driver = webdriver.Firefox(executable_path=pth_geck)

        # driver.wait = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "element_id")))
        driver.wait = WebDriverWait(driver, 15)
        return driver


    def get_price(self, driver):
        n = 0
        while True:
            now = datetime.datetime.now()
            if now.hour == 8 or now.hour == 12 or now.hour == 13 or now.hour == 20 or now.hour == 23 or now.hour == 4:
                try:
                    driver.minimize_window()
                    driver.get(url0)
                    time.sleep(20)
                    driver.get(url1)
                    Treasury = 0
                    Binance = 0
                    Huobi1 = 0
                    Huobi2 = 0
                    Huobi = 0
                    for data in driver.find_elements_by_xpath("//tbody/tr"):
                        if data.text.find("1NTMakcgVwQpMdGxRQnFKyb3G1FAJysSfz") >= 0:
                            u = data.text.split()
                            m = u[1].replace(",", "")
                            Treasury = int(m)
                            print(Treasury)
                            wallet(Treasury, 'Treasury')
                        elif data.text.find("1HckjUpRGcrrRAtFaaCAUaGjsPx9oYmLaZ") >= 0:
                            u = data.text.split()
                            m = u[1].replace(",", "")
                            Huobi1 = int(m)
                            print(Huobi1)
                        elif data.text.find("35hK24tcLEWcgNA4JxpvbkNkoAcDGqQPsP") >= 0:
                            u = data.text.split()
                            m = u[1].replace(",", "")
                            Huobi2 = int(m)
                            print(Huobi2)
                        elif data.text.find("1FoWyxwPXuj4C6abqwhjDWdz6D4PZgYRjA") >= 0:
                            u = data.text.split()
                            m = u[1].replace(",", "")
                            Binance = int(m)
                            print(Binance)
                            wallet(Binance, 'Binance')
                    Huobi = Huobi1 + Huobi2
                    wallet(Huobi, 'Huobi')
                    driver.get(url2)
                    time.sleep(3)
                    data = driver.find_elements_by_xpath("//tr/td")[20]
                    print("Lay du lieu thoi diem: ", datetime.datetime.now())
                    print(
                        "Thoi diem lay du lieu: " + str(datetime.datetime.now()) + "\nTreasury USDT: " + str(
                            Treasury) + "\nBinance USDT: " + str(Binance) + "\nHuobi USDT: " + str(
                            Huobi) + "\nTotal Liabilities " + (data.text))
                    driver.close()
                    time.sleep(4000)
                except:
                    time.sleep(30)
                    n += 1
                    if n % 10 == 0:
                        print(
                            "Neu nhan duoc thong bao nay lien tuc, co loi ket noi toi tether.to, check rdp 52.123")
                    print("Loi lan thu ", n)


if __name__ == '__main__':
    crl = Crawler()
    drivers = crl.init_driver()
    crl.get_price(drivers)
