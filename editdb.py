import datetime
import time
import json
from my_first_project.api_cls import Method

_met = Method

def json_serial(obj):
    """JSON serializer for objects not serializable by default json code"""

    if isinstance(obj, (datetime.datetime)):
        s = obj.isoformat()
        basedate = s.split('.')[0]
        formatfrom = "%Y-%m-%dT%H:%M:%S"
        formatto = "%a, %d %b %Y %H:%M:%S GMT"
        return datetime.datetime.strptime(basedate, formatfrom).strftime(formatto)

def wallet(wal, wal_name):
    data = {
        "coin_name": "USDT",
        "wallet": wal_name,
        "description": "sonpc1",
        "number": [
            {
                "number": wal,
                "date_time": json_serial(datetime.datetime.now()),
                "price_btc": 0,
                "price_usd": 0
            }]
    }
    url_wallet = 'http://210.211.97.169:8061/wallet_number/?where={"wallet":"%s"}' % wal_name
    headers = {'Content-Type': 'application/json'}
    if len((json.loads(_met.get_cls(url_wallet).decode('utf-8')))['_items']) == 0:
        _met.post_cls(url_wallet, data, headers)
    else:
        for row in (json.loads(_met.get_cls(url_wallet).decode('utf-8')))['_items']:
            key = False
            for j in row['number']:
                if data == j:
                    key = True
                    break
            if key:
                break
            else:
                for i in row['number']:
                    data['number'].append(i)
                print(data['number'])
                url_patch = 'http://210.211.97.169:8061/wallet_number/%s' % row['_id']
                headers = {'If-Match': row['_etag'], 'Content-Type': 'application/json'}
                _met.patch_cls(url_patch, data, headers)