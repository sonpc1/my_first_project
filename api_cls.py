import datetime
import time
import requests
import json


class Method:
    def __init__(self):
        pass

    @staticmethod
    def get_cls(url):
        res_get = requests.get(url)
        print(res_get.status_code)
        return res_get.content

    @staticmethod
    def post_cls(url, data, headers1):
        try:
            res_post = requests.post(url, json=data, headers=headers1)
            print(res_post.status_code)
        except BaseException as err:
            print(err)
        return res_post.content

    @staticmethod
    def patch_cls(url, data, headers):
        res_patch = requests.patch(url, json=data, headers=headers)
        print(res_patch.status_code)
        return res_patch.content

    @staticmethod
    def getdata(url):
        res_get = requests.get(url)
        coin_num = json.loads(res_get.content.decode('utf-8'))['_items'][0]['number']
        number = []
        dt = []
        wal_name = []
        for row in range(len(coin_num) - 1, -1, -1):
            number.append(coin_num[row]['number'])
            dt.append(coin_num[row]['date_time'])
            wal_name.append(json.loads(res_get.content.decode('utf-8'))['_items'][0]['wallet'])
        return number, dt, wal_name

if __name__ == '__main__':
    url = 'http://210.211.97.169:8061/wallet_number/?where={"wallet": "Binance"}'
    s = Method.getdata(url)
    print(s)
    for row in s:
        print(row)